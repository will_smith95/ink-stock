﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.txt_ink = New System.Windows.Forms.TextBox()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.btn_SearchSwitch = New MetroFramework.Controls.MetroButton()
        Me.btn_add = New MetroFramework.Controls.MetroButton()
        Me.btn_all = New MetroFramework.Controls.MetroButton()
        Me.btn_clear = New MetroFramework.Controls.MetroButton()
        Me.btn_search = New MetroFramework.Controls.MetroButton()
        Me.lbl_Search = New System.Windows.Forms.Label()
        Me.lbl_Printer = New System.Windows.Forms.Label()
        Me.txt_Printer = New System.Windows.Forms.TextBox()
        Me.btn_updateCancel = New MetroFramework.Controls.MetroButton()
        Me.btn_updateConfirm = New MetroFramework.Controls.MetroButton()
        Me.btn_update = New MetroFramework.Controls.MetroButton()
        Me.btn_reqDecrease = New MetroFramework.Controls.MetroButton()
        Me.btn_reqIncrease = New MetroFramework.Controls.MetroButton()
        Me.btn_qtyIncrease = New MetroFramework.Controls.MetroButton()
        Me.btn_qtyDecrease = New MetroFramework.Controls.MetroButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lbl_ink = New System.Windows.Forms.Label()
        Me.txt_required = New System.Windows.Forms.TextBox()
        Me.txt_quantity = New System.Windows.Forms.TextBox()
        Me.txt_colour = New System.Windows.Forms.TextBox()
        Me.txt_inkCode = New System.Windows.Forms.TextBox()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_provider = New System.Windows.Forms.TextBox()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txt_ink
        '
        Me.txt_ink.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_ink.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ink.Location = New System.Drawing.Point(15, 46)
        Me.txt_ink.Name = "txt_ink"
        Me.txt_ink.Size = New System.Drawing.Size(174, 26)
        Me.txt_ink.TabIndex = 1
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.btn_SearchSwitch)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btn_add)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btn_all)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btn_clear)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btn_search)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lbl_Search)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txt_ink)
        Me.SplitContainer1.Panel1.Cursor = System.Windows.Forms.Cursors.Default
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txt_provider)
        Me.SplitContainer1.Panel2.Controls.Add(Me.lbl_Printer)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txt_Printer)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btn_updateCancel)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btn_updateConfirm)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btn_update)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btn_reqDecrease)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btn_reqIncrease)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btn_qtyIncrease)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btn_qtyDecrease)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.lbl_ink)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txt_required)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txt_quantity)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txt_colour)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txt_inkCode)
        Me.SplitContainer1.Panel2.Controls.Add(Me.BindingNavigator1)
        Me.SplitContainer1.Size = New System.Drawing.Size(560, 436)
        Me.SplitContainer1.SplitterDistance = 85
        Me.SplitContainer1.TabIndex = 6
        '
        'btn_SearchSwitch
        '
        Me.btn_SearchSwitch.FontWeight = MetroFramework.MetroButtonWeight.Regular
        Me.btn_SearchSwitch.Highlight = True
        Me.btn_SearchSwitch.Location = New System.Drawing.Point(255, 14)
        Me.btn_SearchSwitch.Name = "btn_SearchSwitch"
        Me.btn_SearchSwitch.Size = New System.Drawing.Size(133, 26)
        Me.btn_SearchSwitch.Style = MetroFramework.MetroColorStyle.Teal
        Me.btn_SearchSwitch.TabIndex = 23
        Me.btn_SearchSwitch.TabStop = False
        Me.btn_SearchSwitch.Text = "Search Printers"
        Me.btn_SearchSwitch.UseSelectable = True
        '
        'btn_add
        '
        Me.btn_add.FontWeight = MetroFramework.MetroButtonWeight.Regular
        Me.btn_add.Highlight = True
        Me.btn_add.Location = New System.Drawing.Point(445, 46)
        Me.btn_add.Name = "btn_add"
        Me.btn_add.Size = New System.Drawing.Size(80, 26)
        Me.btn_add.Style = MetroFramework.MetroColorStyle.Teal
        Me.btn_add.TabIndex = 22
        Me.btn_add.TabStop = False
        Me.btn_add.Text = "Add New Ink"
        Me.btn_add.UseSelectable = True
        '
        'btn_all
        '
        Me.btn_all.FontWeight = MetroFramework.MetroButtonWeight.Regular
        Me.btn_all.Highlight = True
        Me.btn_all.Location = New System.Drawing.Point(445, 14)
        Me.btn_all.Name = "btn_all"
        Me.btn_all.Size = New System.Drawing.Size(61, 26)
        Me.btn_all.Style = MetroFramework.MetroColorStyle.Silver
        Me.btn_all.TabIndex = 21
        Me.btn_all.TabStop = False
        Me.btn_all.Text = "View All"
        Me.btn_all.UseSelectable = True
        '
        'btn_clear
        '
        Me.btn_clear.Enabled = False
        Me.btn_clear.FontWeight = MetroFramework.MetroButtonWeight.Regular
        Me.btn_clear.Highlight = True
        Me.btn_clear.Location = New System.Drawing.Point(327, 46)
        Me.btn_clear.Name = "btn_clear"
        Me.btn_clear.Size = New System.Drawing.Size(61, 26)
        Me.btn_clear.Style = MetroFramework.MetroColorStyle.Black
        Me.btn_clear.TabIndex = 20
        Me.btn_clear.TabStop = False
        Me.btn_clear.Text = "Clear"
        Me.btn_clear.UseSelectable = True
        '
        'btn_search
        '
        Me.btn_search.FontWeight = MetroFramework.MetroButtonWeight.Regular
        Me.btn_search.Highlight = True
        Me.btn_search.Location = New System.Drawing.Point(255, 46)
        Me.btn_search.Name = "btn_search"
        Me.btn_search.Size = New System.Drawing.Size(61, 26)
        Me.btn_search.Style = MetroFramework.MetroColorStyle.Teal
        Me.btn_search.TabIndex = 19
        Me.btn_search.TabStop = False
        Me.btn_search.Text = "Search"
        Me.btn_search.UseSelectable = True
        '
        'lbl_Search
        '
        Me.lbl_Search.AutoSize = True
        Me.lbl_Search.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Search.Location = New System.Drawing.Point(14, 24)
        Me.lbl_Search.Name = "lbl_Search"
        Me.lbl_Search.Size = New System.Drawing.Size(80, 19)
        Me.lbl_Search.TabIndex = 18
        Me.lbl_Search.Text = "Search Ink"
        '
        'lbl_Printer
        '
        Me.lbl_Printer.AutoSize = True
        Me.lbl_Printer.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Printer.Location = New System.Drawing.Point(43, 145)
        Me.lbl_Printer.Name = "lbl_Printer"
        Me.lbl_Printer.Size = New System.Drawing.Size(57, 19)
        Me.lbl_Printer.TabIndex = 31
        Me.lbl_Printer.Text = "Printer"
        '
        'txt_Printer
        '
        Me.txt_Printer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Printer.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Printer.Location = New System.Drawing.Point(106, 146)
        Me.txt_Printer.Multiline = True
        Me.txt_Printer.Name = "txt_Printer"
        Me.txt_Printer.ReadOnly = True
        Me.txt_Printer.Size = New System.Drawing.Size(166, 98)
        Me.txt_Printer.TabIndex = 30
        Me.txt_Printer.TabStop = False
        '
        'btn_updateCancel
        '
        Me.btn_updateCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.btn_updateCancel.FontWeight = MetroFramework.MetroButtonWeight.Regular
        Me.btn_updateCancel.Highlight = True
        Me.btn_updateCancel.Location = New System.Drawing.Point(331, 285)
        Me.btn_updateCancel.Name = "btn_updateCancel"
        Me.btn_updateCancel.Size = New System.Drawing.Size(106, 43)
        Me.btn_updateCancel.Style = MetroFramework.MetroColorStyle.Red
        Me.btn_updateCancel.TabIndex = 29
        Me.btn_updateCancel.TabStop = False
        Me.btn_updateCancel.Text = "Cancel Update"
        Me.btn_updateCancel.UseSelectable = True
        Me.btn_updateCancel.Visible = False
        '
        'btn_updateConfirm
        '
        Me.btn_updateConfirm.FontSize = MetroFramework.MetroButtonSize.Medium
        Me.btn_updateConfirm.FontWeight = MetroFramework.MetroButtonWeight.Regular
        Me.btn_updateConfirm.Highlight = True
        Me.btn_updateConfirm.Location = New System.Drawing.Point(230, 285)
        Me.btn_updateConfirm.Name = "btn_updateConfirm"
        Me.btn_updateConfirm.Size = New System.Drawing.Size(84, 43)
        Me.btn_updateConfirm.Style = MetroFramework.MetroColorStyle.Green
        Me.btn_updateConfirm.TabIndex = 28
        Me.btn_updateConfirm.TabStop = False
        Me.btn_updateConfirm.Text = "Confirm"
        Me.btn_updateConfirm.UseSelectable = True
        Me.btn_updateConfirm.Visible = False
        '
        'btn_update
        '
        Me.btn_update.FontSize = MetroFramework.MetroButtonSize.Medium
        Me.btn_update.FontWeight = MetroFramework.MetroButtonWeight.Regular
        Me.btn_update.Highlight = True
        Me.btn_update.Location = New System.Drawing.Point(135, 285)
        Me.btn_update.Name = "btn_update"
        Me.btn_update.Size = New System.Drawing.Size(79, 43)
        Me.btn_update.Style = MetroFramework.MetroColorStyle.Teal
        Me.btn_update.TabIndex = 27
        Me.btn_update.TabStop = False
        Me.btn_update.Text = "Update"
        Me.btn_update.UseSelectable = True
        Me.btn_update.Visible = False
        '
        'btn_reqDecrease
        '
        Me.btn_reqDecrease.Enabled = False
        Me.btn_reqDecrease.FontSize = MetroFramework.MetroButtonSize.Medium
        Me.btn_reqDecrease.Highlight = True
        Me.btn_reqDecrease.Location = New System.Drawing.Point(458, 120)
        Me.btn_reqDecrease.Name = "btn_reqDecrease"
        Me.btn_reqDecrease.Size = New System.Drawing.Size(22, 20)
        Me.btn_reqDecrease.Style = MetroFramework.MetroColorStyle.Red
        Me.btn_reqDecrease.TabIndex = 26
        Me.btn_reqDecrease.TabStop = False
        Me.btn_reqDecrease.Text = "<"
        Me.btn_reqDecrease.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_reqDecrease.UseSelectable = True
        '
        'btn_reqIncrease
        '
        Me.btn_reqIncrease.Enabled = False
        Me.btn_reqIncrease.FontSize = MetroFramework.MetroButtonSize.Medium
        Me.btn_reqIncrease.Highlight = True
        Me.btn_reqIncrease.Location = New System.Drawing.Point(503, 120)
        Me.btn_reqIncrease.Name = "btn_reqIncrease"
        Me.btn_reqIncrease.Size = New System.Drawing.Size(22, 20)
        Me.btn_reqIncrease.Style = MetroFramework.MetroColorStyle.Green
        Me.btn_reqIncrease.TabIndex = 25
        Me.btn_reqIncrease.TabStop = False
        Me.btn_reqIncrease.Text = ">"
        Me.btn_reqIncrease.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_reqIncrease.UseSelectable = True
        '
        'btn_qtyIncrease
        '
        Me.btn_qtyIncrease.Enabled = False
        Me.btn_qtyIncrease.FontSize = MetroFramework.MetroButtonSize.Medium
        Me.btn_qtyIncrease.Highlight = True
        Me.btn_qtyIncrease.Location = New System.Drawing.Point(368, 120)
        Me.btn_qtyIncrease.Name = "btn_qtyIncrease"
        Me.btn_qtyIncrease.Size = New System.Drawing.Size(22, 20)
        Me.btn_qtyIncrease.Style = MetroFramework.MetroColorStyle.Green
        Me.btn_qtyIncrease.TabIndex = 24
        Me.btn_qtyIncrease.TabStop = False
        Me.btn_qtyIncrease.Text = ">"
        Me.btn_qtyIncrease.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_qtyIncrease.UseSelectable = True
        '
        'btn_qtyDecrease
        '
        Me.btn_qtyDecrease.Enabled = False
        Me.btn_qtyDecrease.FontSize = MetroFramework.MetroButtonSize.Medium
        Me.btn_qtyDecrease.Highlight = True
        Me.btn_qtyDecrease.Location = New System.Drawing.Point(327, 120)
        Me.btn_qtyDecrease.Name = "btn_qtyDecrease"
        Me.btn_qtyDecrease.Size = New System.Drawing.Size(22, 20)
        Me.btn_qtyDecrease.Style = MetroFramework.MetroColorStyle.Red
        Me.btn_qtyDecrease.TabIndex = 23
        Me.btn_qtyDecrease.TabStop = False
        Me.btn_qtyDecrease.Text = "<"
        Me.btn_qtyDecrease.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_qtyDecrease.UseSelectable = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(453, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 19)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Required"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(323, 98)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 19)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Quantity"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(46, 104)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 19)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Colour"
        '
        'lbl_ink
        '
        Me.lbl_ink.AutoSize = True
        Me.lbl_ink.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ink.Location = New System.Drawing.Point(32, 61)
        Me.lbl_ink.Name = "lbl_ink"
        Me.lbl_ink.Size = New System.Drawing.Size(68, 19)
        Me.lbl_ink.TabIndex = 9
        Me.lbl_ink.Text = "Ink Code"
        '
        'txt_required
        '
        Me.txt_required.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_required.Location = New System.Drawing.Point(458, 146)
        Me.txt_required.Name = "txt_required"
        Me.txt_required.ReadOnly = True
        Me.txt_required.Size = New System.Drawing.Size(67, 26)
        Me.txt_required.TabIndex = 8
        Me.txt_required.TabStop = False
        '
        'txt_quantity
        '
        Me.txt_quantity.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_quantity.Location = New System.Drawing.Point(327, 146)
        Me.txt_quantity.Name = "txt_quantity"
        Me.txt_quantity.ReadOnly = True
        Me.txt_quantity.Size = New System.Drawing.Size(63, 26)
        Me.txt_quantity.TabIndex = 7
        Me.txt_quantity.TabStop = False
        '
        'txt_colour
        '
        Me.txt_colour.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_colour.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_colour.Location = New System.Drawing.Point(106, 100)
        Me.txt_colour.Name = "txt_colour"
        Me.txt_colour.ReadOnly = True
        Me.txt_colour.Size = New System.Drawing.Size(166, 26)
        Me.txt_colour.TabIndex = 6
        Me.txt_colour.TabStop = False
        '
        'txt_inkCode
        '
        Me.txt_inkCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_inkCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_inkCode.Location = New System.Drawing.Point(106, 57)
        Me.txt_inkCode.Name = "txt_inkCode"
        Me.txt_inkCode.ReadOnly = True
        Me.txt_inkCode.Size = New System.Drawing.Size(166, 26)
        Me.txt_inkCode.TabIndex = 5
        Me.txt_inkCode.TabStop = False
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Me.BindingNavigatorCountItem
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Enabled = False
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.BindingNavigator1.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.BindingNavigator1.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.BindingNavigator1.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.BindingNavigatorPositionItem
        Me.BindingNavigator1.Size = New System.Drawing.Size(560, 25)
        Me.BindingNavigator1.TabIndex = 0
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(323, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 19)
        Me.Label4.TabIndex = 33
        Me.Label4.Text = "Provider"
        '
        'txt_provider
        '
        Me.txt_provider.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_provider.Location = New System.Drawing.Point(327, 57)
        Me.txt_provider.Name = "txt_provider"
        Me.txt_provider.ReadOnly = True
        Me.txt_provider.Size = New System.Drawing.Size(198, 26)
        Me.txt_provider.TabIndex = 32
        Me.txt_provider.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(560, 436)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ink Stock"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txt_ink As System.Windows.Forms.TextBox
    Friend WithEvents BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents txt_required As System.Windows.Forms.TextBox
    Friend WithEvents txt_quantity As System.Windows.Forms.TextBox
    Friend WithEvents txt_colour As System.Windows.Forms.TextBox
    Friend WithEvents txt_inkCode As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lbl_ink As System.Windows.Forms.Label
    Friend WithEvents lbl_Search As System.Windows.Forms.Label
    Friend WithEvents btn_clear As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_search As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_add As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_all As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_updateCancel As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_updateConfirm As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_update As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_reqDecrease As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_reqIncrease As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_qtyIncrease As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_qtyDecrease As MetroFramework.Controls.MetroButton
    Friend WithEvents lbl_Printer As System.Windows.Forms.Label
    Friend WithEvents txt_Printer As System.Windows.Forms.TextBox
    Friend WithEvents btn_SearchSwitch As MetroFramework.Controls.MetroButton
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_provider As TextBox
End Class
