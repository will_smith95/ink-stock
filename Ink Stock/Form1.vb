﻿Imports System.Text
Imports System.IO
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports MetroFramework.Forms.MetroForm
Imports System.Net.Mime.MediaTypeNames
Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices

Public Class Form1

    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Init()
    End Sub

    Private Sub Init()

        AddHandler btn_add.Click, AddressOf Add
        AddHandler btn_search.Click, AddressOf Search
        AddHandler btn_all.Click, AddressOf All
        AddHandler btn_clear.Click, AddressOf Clear
        AddHandler btn_update.Click, AddressOf UpdateInk
        AddHandler btn_qtyIncrease.Click, AddressOf QuantityInc
        AddHandler btn_qtyDecrease.Click, AddressOf QuantityDec
        AddHandler btn_reqIncrease.Click, AddressOf RequiredInc
        AddHandler btn_reqDecrease.Click, AddressOf RequiredDec
        AddHandler btn_updateConfirm.Click, AddressOf UpdateConfirm
        AddHandler btn_updateCancel.Click, AddressOf UpdateCancel
        AddHandler btn_SearchSwitch.Click, AddressOf SearchSwitch
        AddHandler txt_ink.KeyDown, AddressOf EnterSearch
        AddHandler Me.KeyDown, AddressOf EscClear
    End Sub

    Public oldInk As String
    Dim quantityCount As Integer
    Dim requiredCount As Integer
    Dim alertInk As String
    Dim alertRequired As String
    Dim PrintSearch As Boolean = False

    Private Sub EnterSearch(sender As Object, e As KeyEventArgs)

        If e.KeyCode = Keys.Enter Then

            Search()
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub EscClear(sender As Object, e As KeyEventArgs)

        If e.KeyCode = Keys.Escape Then

            Clear()
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub Alerts()

        Dim objMissing As Object = Type.Missing
        Dim objOutlook As Outlook.Application = Nothing
        Dim objNS As Outlook.NameSpace = Nothing
        Dim objMail As Outlook.MailItem = Nothing

        Dim url As String = ""

        If alertInk.Trim.StartsWith("C") Then

            url = "http://www.cartridgesave.co.uk/" + alertInk.Trim
        Else
            url = "http://www.cartridgesave.co.uk/"
        End If

        Try
            ' Start Microsoft Outlook and log on with your profile.
            ' Create an Outlook application.
            objOutlook = New Outlook.Application()

            ' Get the namespace
            objNS = objOutlook.GetNamespace("MAPI")

            ' Log on by using a dialog box to choose the profile.
            objNS.Logon(objMissing, objMissing, False, False)

            ' create an email message
            objMail = CType(objOutlook.CreateItem(Outlook.OlItemType.olMailItem), Outlook.MailItem)

            ' Set the properties of the email.
            With objMail
                .Subject = "Out Of " + alertInk + " Ink!"
                .To = "ITSupport@pricecheck.uk.com"
                .Body = "You have no more stock of: " + alertInk + vbCr + vbCr + "You need to order " + alertRequired + " more from: " + url + vbCr + vbCr + "Thanks, "
                .Send()
            End With

        Catch ex As Exception

        Finally

            If Not objMail Is Nothing Then
                Marshal.FinalReleaseComObject(objMail)
                objMail = Nothing

            End If

            If Not objNS Is Nothing Then
                Marshal.FinalReleaseComObject(objNS)
                objNS = Nothing

            End If
            If Not objOutlook Is Nothing Then
                Marshal.FinalReleaseComObject(objOutlook)
                objOutlook = Nothing

            End If

        End Try

        MsgBox("You Have No Stock Of: " + alertInk + vbCr + vbCr + "An Email Has Been Sent To You With Further Details!")

        If alertInk.Trim.StartsWith("C") Then

            Process.Start(url)
        End If

    End Sub

    Private Sub pushData(ByVal type As String)

        'Places all Values of shown record into new Variables
        Dim inkCode_update As String = sanitize(txt_inkCode.Text.Trim)
        Dim colour_update As String = sanitize(txt_colour.Text.Trim)
        Dim quantity_update As String = sanitize(txt_quantity.Text.Trim)
        Dim required_update As String = sanitize(txt_required.Text.Trim)
        Dim printer_update As String = sanitize(txt_Printer.Text.Trim)
        Dim provider_update As String = sanitize(txt_provider.Text.Trim)

        'New connection to storedProcedure to update details to
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(db_conx(My.Settings.databaseCred))

        Try
            'Opens new database connection
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure

            If type = "Update" Then

                'Need the info on updating it's in "storedProcedure name here"
                command.CommandText = "UpdateInkAudit"
                command.Parameters.AddWithValue("@oldInk", oldInk)

            ElseIf type = "Insert" Then

                'Need the info on updating it's in "storedProcedure name here"
                command.CommandText = "InsertInkAudit"
            End If

            'Updates the info entered into the following @variables in the SQL storedProcedure
            command.Parameters.AddWithValue("@inkCode", inkCode_update)
            command.Parameters.AddWithValue("@colour", colour_update)
            command.Parameters.AddWithValue("@quantity", quantity_update)
            command.Parameters.AddWithValue("@required", required_update)
            command.Parameters.AddWithValue("@printer", printer_update)
            command.Parameters.AddWithValue("@provider", provider_update)

            'Executes without a return
            command.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception

            MsgBox("There appears to be a problem - Please contact IT to resolve: " & vbCr + ex.ToString, 0, "Error")
        End Try

        If type = "Update" Then

            MsgBox("All changes have been saved!", 0, "Updated Ink")
        Else

            MsgBox("New Ink added", 0, "Added Ink")
        End If

        Clear()
    End Sub

    Private Sub SearchSwitch()

        If PrintSearch = False Then

            lbl_Search.Text = "Search Printer"
            btn_SearchSwitch.Text = "Search Ink"
            PrintSearch = True
        Else
            lbl_Search.Text = "Search Ink"
            btn_SearchSwitch.Text = "Search Printer"
            PrintSearch = False
        End If

        txt_ink.Focus()
    End Sub

    Private Sub Search()

        Dim textboxValue As String = txt_ink.Text.Trim
        textboxValue = sanitize(textboxValue)

        If PrintSearch = False Then

            Try
                If textboxValue = "" Then
                    MsgBox("Please input Ink Code!", 0, "Ink Input")
                    Return
                End If

                btn_search.Enabled = False
                txt_ink.Enabled = False

                'New variable ds type DataSet = function from Database("name_storedProcedure, variable in DBC region(Settings Pass), "field (variable specified in db)", value (variable holding input)
                Dim ds As DataSet = getDataSetFromPTLSAGE("GetInkAudit", db_conx(My.Settings.databaseCred), "@inkCode", textboxValue)

                If ds.Tables(0).Rows.Count > 0 Then

                    'Binds the data from Tables(0) = 1st table in dataset
                    BindingSource1.DataSource = ds.Tables(0)

                    'Sets which dataset to navigate through
                    BindingNavigator1.BindingSource = BindingSource1
                    controlBinding(BindingSource1)

                    'Allow functioning of navigator
                    BindingNavigator1.Enabled = True

                    'Enables the Clear button
                    btn_clear.Enabled = True

                    'Disables the search and view all
                    btn_all.Enabled = False
                    btn_add.Enabled = False
                    btn_search.Enabled = False

                    'Prevents overtyping on search
                    txt_ink.ReadOnly = True

                    'Enables the Navigator
                    BindingNavigator1.Enabled = True

                    'Makes Update button Visible
                    btn_update.Visible = True

                Else

                    MsgBox("No Ink Found", 0, "Failed")
                    btn_search.Enabled = True
                    txt_ink.Enabled = True

                End If

            Catch ex As Exception

                MsgBox("There appears to be a problem - Please contact IT to resolve: " & vbCr & vbCr + ex.ToString, 0, "Error")

            End Try

        Else

            Try
                If textboxValue = "" Then
                    MsgBox("Please input valid Printer!", 0, "Print Input")
                    Return
                End If

                btn_search.Enabled = False
                txt_ink.Enabled = False

                'New variable ds type DataSet = function from Database("name_storedProcedure, variable in DBC region(Settings Pass), "field (variable specified in db)", value (variable holding input)
                Dim ds As DataSet = getDataSetFromPTLSAGE("GetInkAudit_Printer", db_conx(My.Settings.databaseCred), "@printer", textboxValue)

                If ds.Tables(0).Rows.Count > 0 Then

                    'Binds the data from Tables(0) = 1st table in dataset
                    BindingSource1.DataSource = ds.Tables(0)

                    'Sets which dataset to navigate through
                    BindingNavigator1.BindingSource = BindingSource1
                    controlBinding(BindingSource1)

                    'Allow functioning of navigator
                    BindingNavigator1.Enabled = True

                    'Enables the Clear button
                    btn_clear.Enabled = True

                    'Disables the search and view all
                    btn_all.Enabled = False
                    btn_add.Enabled = False
                    btn_search.Enabled = False

                    'Prevents overtyping on search
                    txt_ink.ReadOnly = True

                    'Enables the Navigator
                    BindingNavigator1.Enabled = True

                    'Makes Update button Visible
                    btn_update.Visible = True

                Else

                    MsgBox("No Printer Found", 0, "Failed")
                    btn_search.Enabled = True
                    txt_ink.Enabled = True

                End If

            Catch ex As Exception

                MsgBox("There appears to be a problem - Please contact IT to resolve: " & vbCr & vbCr + ex.ToString, 0, "Error")

            End Try

        End If
    End Sub

    Private Sub All()

        btn_search.Enabled = False
        txt_ink.Enabled = False

        Try

            'New variable ds type DataSet = function from Database("name_storedProcedure, variable in DBC region(Settings Pass), "field (variable specified in db)", value (variable holding input)
            Dim ds As DataSet = getDataSetFromPTLSAGE("GetInkAudit", db_conx(My.Settings.databaseCred), "@inkCode", " ")

            If ds.Tables(0).Rows.Count > 0 Then

                'Binds the data from Tables(0) = 1st table in dataset
                BindingSource1.DataSource = ds.Tables(0)

                'Sets which dataset to navigate through
                BindingNavigator1.BindingSource = BindingSource1
                controlBinding(BindingSource1)

                'Allow functioning of navigator
                BindingNavigator1.Enabled = True

                'Enables the Clear button
                btn_clear.Enabled = True

                'Visible
                btn_update.Visible = True

                'Disables the search and view all
                btn_all.Enabled = False
                btn_add.Enabled = False
                btn_search.Enabled = False

                'Prevents overtyping on search
                txt_ink.ReadOnly = True

                'Enables the Navigator
                BindingNavigator1.Enabled = True

            Else

                MsgBox("No Ink Found", 0, "Failed")
                btn_search.Enabled = True
                txt_ink.Enabled = True

            End If

        Catch ex As Exception

            MsgBox("Error - Call IT for Assistance: " & vbCr & vbCr & ex.ToString)
        End Try

    End Sub

    Private Sub Clear()

        Try
            'Clears the Textbox
            txt_ink.Text = ""
            btn_updateConfirm.Text = "Confirm"
            btn_updateCancel.Text = "Cancel Update"

            'Re-Enables the Textboxes
            btn_search.Enabled = True
            txt_ink.Enabled = True

            'Clears the binding of info from table
            txt_inkCode.DataBindings.Clear()
            txt_colour.DataBindings.Clear()
            txt_quantity.DataBindings.Clear()
            txt_required.DataBindings.Clear()
            txt_Printer.DataBindings.Clear()
            txt_provider.DataBindings.Clear()

            'Clears the Textboxes
            txt_inkCode.Text = ""
            txt_colour.Text = ""
            txt_quantity.Text = ""
            txt_required.Text = ""
            txt_Printer.Text = ""
            txt_provider.Text = ""

            'Refreshes the Binding Nav and DataSource
            BindingNavigator1.BindingSource = Nothing
            BindingSource1.DataSource = Nothing

            'Sets all textboxes to read only 
            txt_inkCode.ReadOnly = True
            txt_colour.ReadOnly = True
            txt_quantity.ReadOnly = True
            txt_required.ReadOnly = True
            txt_Printer.ReadOnly = True
            txt_provider.ReadOnly = True

            'Hides the record function buttons
            btn_update.Visible = False
            btn_updateConfirm.Visible = False
            btn_updateCancel.Visible = False

            'Enable the add, view all, update button
            btn_add.Enabled = True
            btn_all.Enabled = True
            btn_update.Enabled = True

            'Disables the Clear and increase/decrease buttons
            btn_clear.Enabled = False
            btn_qtyDecrease.Enabled = False
            btn_qtyIncrease.Enabled = False
            btn_reqDecrease.Enabled = False
            btn_reqIncrease.Enabled = False

            'Prevents overtyping on search
            txt_ink.ReadOnly = False

            'Re-Colours the boxes
            txt_inkCode.BackColor = Nothing
            txt_colour.BackColor = Nothing
            txt_required.BackColor = Nothing
            txt_Printer.BackColor = Nothing
            txt_provider.BackColor = Nothing
        Catch ex As Exception

            MsgBox("There appears to be a problem - Please contact IT to resolve: " & vbCr & vbCr + ex.ToString, 0, "Error")
        End Try
    End Sub

    Private Sub Add()

        'Sets the Value of qty and req
        txt_quantity.Text = 0
        txt_required.Text = 0
        quantityCount = 0
        requiredCount = 0

        'Renames the Confirm/Cancel Button
        btn_updateConfirm.Text = "Save"
        btn_updateCancel.Text = "Cancel Add"

        'Sets all textboxes and checkboxes able to edit 
        txt_inkCode.ReadOnly = False
        txt_colour.ReadOnly = False
        txt_Printer.ReadOnly = False
        txt_provider.ReadOnly = False

        'Allows editing of quantity and required
        btn_qtyIncrease.Enabled = True
        btn_reqIncrease.Enabled = True

        'Shows save/cancel
        btn_updateConfirm.Visible = True
        btn_updateCancel.Visible = True

        'Disables the search and view all
        btn_all.Enabled = False
        btn_add.Enabled = False
        btn_search.Enabled = False
    End Sub

    Private Sub QuantityInc()

        If btn_qtyDecrease.Enabled = False Then
            btn_qtyDecrease.Enabled = True
        End If

        quantityCount = quantityCount + 1
        txt_quantity.Text = quantityCount
    End Sub

    Private Sub QuantityDec()

        If quantityCount - 1 <= 0 Then
            btn_qtyDecrease.Enabled = False
        End If

        quantityCount = quantityCount - 1
        txt_quantity.Text = quantityCount
    End Sub

    Private Sub RequiredInc()

        If btn_reqDecrease.Enabled = False Then
            btn_reqDecrease.Enabled = True
        End If

        requiredCount = requiredCount + 1
        txt_required.Text = requiredCount

    End Sub

    Private Sub RequiredDec()

        If requiredCount - 1 <= 0 Then
            btn_reqDecrease.Enabled = False
        End If

        requiredCount = requiredCount - 1
        txt_required.Text = requiredCount

    End Sub

    Private Sub UpdateInk()

        quantityCount = txt_quantity.Text
        requiredCount = txt_required.Text

        'Prevents the user scrolling through records while editing
        BindingNavigator1.Enabled = False

        'Old username is used to update the new details to the database
        oldInk = txt_inkCode.Text.Trim

        'Allows Incrementing and Decrementing
        btn_qtyIncrease.Enabled = True
        btn_reqIncrease.Enabled = True

        'Allows Editing of Printer box
        txt_Printer.ReadOnly = False
        txt_provider.ReadOnly = False

        If quantityCount = 0 Then
            btn_qtyDecrease.Enabled = False
        Else
            btn_qtyDecrease.Enabled = True
        End If

        If requiredCount = 0 Then
            btn_reqDecrease.Enabled = False
        Else
            btn_reqDecrease.Enabled = True
        End If

        'Disables the update button
        btn_update.Enabled = False
        btn_clear.Enabled = False

        'Makes the Save button visible
        btn_updateConfirm.Visible = True
        btn_updateCancel.Visible = True
    End Sub

    Private Sub UpdateConfirm()

        quantityCount = Convert.ToInt32(txt_quantity.Text)
        requiredCount = Convert.ToInt32(txt_required.Text)

        If btn_updateConfirm.Text = "Save" Then

            'Declared variable 'errors' that collects all errors in submission as String
            Dim errors As String = ""
            Dim validateErrors As String = "There are errors in your submission: "

            'Places all Values of shown record into new Variables
            Dim inkCode_insert As String = sanitize(txt_inkCode.Text.Trim)
            Dim colour_insert As String = sanitize(txt_colour.Text.Trim)
            Dim quantity_insert As String = sanitize(txt_quantity.Text.Trim)
            Dim required_insert As String = sanitize(txt_required.Text.Trim)
            Dim printer_insert As String = sanitize(txt_Printer.Text.Trim)
            Dim provider_insert As String = sanitize(txt_provider.Text.Trim)

            'Re-Colours the boxes
            txt_inkCode.BackColor = Nothing
            txt_colour.BackColor = Nothing
            txt_required.BackColor = Nothing
            txt_Printer.BackColor = Nothing
            txt_provider.BackColor = Nothing

            Try

                If inkCode_insert = "" Then

                    errors = vbCr + "Please enter a valid Ink Code!"

                    txt_inkCode.BackColor = Color.LightSalmon

                End If

                If colour_insert = "" Then

                    errors = errors + vbCr + "Please enter Colour!"

                    txt_colour.BackColor = Color.LightSalmon

                End If

                If required_insert = 0 Then

                    errors = errors + vbCr + "Please enter Required Stock!"

                    txt_required.BackColor = Color.LightSalmon

                End If

                If printer_insert = "" Then

                    errors = errors + vbCr + "Please enter a valid Printer!"

                    txt_Printer.BackColor = Color.LightSalmon

                End If

                If provider_insert = "" Then

                    errors = errors + vbCr + "Please enter a valid Provider!"

                    txt_provider.BackColor = Color.LightSalmon

                End If

                If errors = "" Then

                    pushData("Insert")

                    btn_updateConfirm.Visible = False

                    btn_updateCancel.Visible = False
                Else

                    MsgBox(validateErrors & vbCr & errors)

                    btn_update.Visible = False

                End If

            Catch ex As Exception

                MsgBox("There appears to be a problem - Please contact IT to resolve: " & vbCr & vbCr + ex.ToString, 0, "Error")

            End Try

        ElseIf quantityCount = 0 Then
            alertInk = Nothing
            alertInk = txt_inkCode.Text
            alertRequired = Nothing
            alertRequired = txt_required.Text

            Alerts()
            pushData("Update")

        ElseIf quantityCount <> requiredCount And quantityCount = 1 Then
            alertInk = Nothing
            alertInk = txt_inkCode.Text
            MsgBox("Ink Stock is getting low: " + alertInk + " = " + quantityCount.ToString + vbCr + vbCr + "Consider ordering more!")

            pushData("Update")
        Else

            pushData("Update")
        End If

    End Sub

    Private Sub UpdateCancel()

        'Re-Enables the following buttons
        btn_update.Enabled = True
        btn_clear.Enabled = True

        'Hides the save and cancel buttons
        btn_updateConfirm.Visible = False
        btn_updateCancel.Visible = False

        'Disables the Increment and Decrement
        btn_qtyIncrease.Enabled = False
        btn_reqIncrease.Enabled = False
        btn_qtyDecrease.Enabled = False
        btn_reqDecrease.Enabled = False

        'Disables Editing of Text
        txt_Printer.ReadOnly = True
        txt_provider.ReadOnly = True

        If btn_updateCancel.Text = "Cancel Add" Then

            Clear()
        Else

            If txt_ink.Text = "" Then

                All()
            Else

                Search()
            End If
        End If

    End Sub

    Private Sub controlBinding(ByVal bs As BindingSource)

        'Clears the binding of info from table
        txt_inkCode.DataBindings.Clear()
        txt_colour.DataBindings.Clear()
        txt_quantity.DataBindings.Clear()
        txt_required.DataBindings.Clear()
        txt_Printer.DataBindings.Clear()
        txt_provider.DataBindings.Clear()

        'Clears the Textboxes
        txt_inkCode.Text = ""
        txt_colour.Text = ""
        txt_quantity.Text = ""
        txt_required.Text = ""
        txt_Printer.Text = ""
        txt_provider.Text = ""

        'Adds the data specified onto the textbox ("type", datasource, "field_in_database", T/F for boolean recognition)
        txt_inkCode.DataBindings.Add(New Binding("Text", bs, "Ink Code", True))
        txt_colour.DataBindings.Add(New Binding("Text", bs, "Colour", True))
        txt_Printer.DataBindings.Add(New Binding("Text", bs, "Printer", True))
        txt_quantity.DataBindings.Add(New Binding("Text", bs, "Quantity", True))
        txt_required.DataBindings.Add(New Binding("Text", bs, "Required", True))
        txt_provider.DataBindings.Add(New Binding("Text", bs, "Provider", True))
    End Sub

    Function getDataSetFromPTLSAGE(ByVal storedProcedure As String, _
                                   ByVal connectionString As String, _
                                   Optional param1Name As String = "", _
                                   Optional param1Val As String = "", _
                                   Optional param2Name As String = "", _
                                   Optional param2Val As String = "", _
                                   Optional param3Name As String = "", _
                                   Optional param3Val As String = "", _
                                   Optional param4Name As String = "", _
                                   Optional param4Val As String = "")

        Dim tempDataset As New DataSet
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)
        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = storedProcedure
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            Dim adapter As New SqlDataAdapter(command)
            adapter.Fill(tempDataset)

        Catch ex As System.Exception
            tempDataset.Tables.Add()
            tempDataset.Tables(0).Rows.Add()
            tempDataset.Tables(0).Columns.Add()

            tempDataset.Tables(0).Rows(0).Item(0) = ex.ToString
            Console.WriteLine(ex.ToString)
        End Try

        Return tempDataset

    End Function

    Public Function db_conx(ByVal hash As String)
        Dim returnValue As String = ""
        If hash = "034cdaf241d87a07c5e5d47261a6f2de" Then
            returnValue = "Data Source=PTLSAGE;Initial Catalog=MPSPriceCheck;User ID=admin;Password=barry250372;"
            Return returnValue
        End If
        Return returnValue
    End Function

    Public Function sanitize(ByVal s As String)
        s = s.Replace("'", "")
        s = s.Replace("--", "")
        s = s.Replace("/*", "")
        s = s.Replace("*/", "")
        s = s.Replace(";", "")
        Return s
    End Function

End Class
